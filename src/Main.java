
import java.io.IOException;
import java.net.*;

public class Main {
    private static boolean isValidIP(InetAddress ip) {
        return ip.isMulticastAddress() && (ip instanceof Inet4Address || ip instanceof Inet6Address);
    }

    public static void main(String[] args) {

        //String groupIP = "230.0.0.0";
        String groupIP = args[0];
        try {
            if (!isValidIP(InetAddress.getByName(groupIP))) {
                System.err.println("Invalid group IP");
                System.exit(1);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            MulticastSocket multicastSocket = new MulticastSocket(4221);
            multicastSocket.joinGroup(InetAddress.getByName(groupIP));

            DatagramSocket datagramSocket = new DatagramSocket(4221, InetAddress.getLocalHost());

            MulticastSender sender = new MulticastSender(InetAddress.getByName(groupIP), 4221, datagramSocket);
            MulticastReceiver receiver = new MulticastReceiver(multicastSocket);

            sender.start();
            receiver.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
