
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public class MulticastReceiver extends Thread {
    private static final int BUFFER_SIZE = 1024;

    private MulticastSocket socket;
    private byte[] buffer = new byte[BUFFER_SIZE];

    private HashMap<InetAddress, LocalTime> copiesMap = null;

    public MulticastReceiver(MulticastSocket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            copiesMap = new HashMap<>();
            while (true) {
                DatagramPacket packet = new DatagramPacket(buffer, BUFFER_SIZE);
                socket.receive(packet);
                //System.out.println("i got something");
                copiesMap.put(packet.getAddress(), LocalTime.now());

                boolean changes = false;
                Map<InetAddress, LocalTime> addressesToDelete = new HashMap<>();
                for (Map.Entry<InetAddress, LocalTime> entry : copiesMap.entrySet()) {
                    if (LocalTime.now().isAfter(entry.getValue().plusSeconds(15))) {
                        addressesToDelete.put(entry.getKey(), entry.getValue());
                        changes = true;
                    }
                }

                for (Map.Entry<InetAddress, LocalTime> address: addressesToDelete.entrySet()) {
                    copiesMap.remove(address.getKey(), address.getValue());
                }

                if (changes) {
                    for (Map.Entry<InetAddress, LocalTime> entry : copiesMap.entrySet()) {
                        System.out.println(entry.getKey() + " is alive");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}